import sys,os,urllib2,urllib,re,ast,csv

# Pass this function a CSV file and return a list of eISBN values.
def create_isbn_list(csv_file):
    cleaned_list = ((open(csv_file)).readlines()[0]).splitlines()
    return cleaned_list

# Pass this function a list of eISBNs (built by create_isbn_list) and return a list. 
# Each index on the list contains a dictionary with a single key, where the eISBN is the key and the results, in dictionary form, are the key's value.
def make_api_call(cleaned_list):
    call_results_list = []
    for isbn in cleaned_list[1:len(cleaned_list)]:
        call_results_dict = {}
        res_dict = ast.literal_eval(urllib2.urlopen('http://services.penguin.com/rest/v1/json/book/filter/data/get?isbn=' + isbn + '&siteid=301&pageid=1&moduleid=0').read())
        call_results_dict[isbn] = res_dict
        call_results_list.append(call_results_dict)
    return call_results_list

# Pass this function the list built by make_api_call.
# Return a similar list, but with the API results cleaned up.    
def make_results_csv_coherent(call_results_list, expected):
    for index in call_results_list:
        for key in index:
            cleaned_results = {}
            results = index.get(key) 
            if results["FilterCode"] == 1:
                excluded = "yes"
                exclusion_reason = re.split("Filter condition satisfied. ", results["FilterMessage"])[1]
            else:
                excluded = "no"
                exclusion_reason = "none"
            cleaned_results = {"exclusion expected?": expected, "excluded?": excluded, "exclusion reason?": exclusion_reason}
            index[key] = cleaned_results
    return call_results_list
    
# Pass this function the list created by make_results_csv_coherent to write a new CSV file with API results.
def write_results_csv(call_results_list):
    with open('results.csv', 'wb') as csvfile:
        fieldnames = ['eISBN', 'exclusion expected?', 'excluded?', 'exclusion reason?']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for index in call_results_list:
            for key in index:
                writer.writerow({'eISBN': key, 'exclusion expected?': (index.get(key))["exclusion expected?"], 'excluded?': (index.get(key))["excluded?"], 'exclusion reason?': (index.get(key))["exclusion reason?"]})

def main(input_csv, expected_result):
    write_results_csv(make_results_csv_coherent(make_api_call(create_isbn_list(input_csv)), expected_result))

# In order to run this script from the command line, pass:
# sys.argv[1] = the desired CSV for analysis
# sys.argv[2] = "yes" (without quotes) if the eISBNs are expected to have Next Reads pages
#               "no" (without quotes) if the eISBNs are not expected to have Next Reads pages, and will have a filter applied
if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])